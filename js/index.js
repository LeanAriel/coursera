$(function() {
    $("[data-toggle='tooltip']").tooltip();
    $('.carousel').carousel({
        interval: 3000
    });
    $('#contacto').on('show.bs.modal', function (e) {
       console.log('El modal se esta ejecutando');
       $('#modalBtn').removeClass('btn btn-outline-success btn-sm');
       $('#modalBtn').addClass('btn-primary');
    });
});